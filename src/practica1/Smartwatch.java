
package practica1;

import java.util.Scanner;

/**
 *
 * @author Pao
 */
public class Smartwatch extends Dispositivo{
    
    Smartwatch(){
        super();
        this.setTipo(3);
    }
    Smartwatch(String Correo_electronico,String Nombre_del_Dispositivo,boolean Visible,boolean Encendido){
        super(Correo_electronico, Nombre_del_Dispositivo, Visible, Encendido);
        this.setTipo(3);
    }

    @Override
    public String Copiar_texto() {
                String textoCopiado="";
        if(this.estaEncendido()){
              Scanner scan= new Scanner(System.in);
               System.out.println("ingrese el texto a copiar");
               textoCopiado=scan.nextLine();
        }else{
            System.out.println("Dispositivo no puede realizar esta accion");
        }
        return textoCopiado;
    }

    @Override
    public void Pegar_texto(String texto_copiado) {
        System.out.println("Dispositivo no realiza esta accion");
    }

    public String Compartir_documentos() {
        System.out.println("Dispositivo no realiza esta accion");
        return "";
    }
}