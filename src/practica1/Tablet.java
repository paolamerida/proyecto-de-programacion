
package practica1;

import java.util.Scanner;

/**
 *
 * @author Pao
 */
public class Tablet extends Dispositivo{
    
    Tablet(){
        super();
        this.setTipo(2);
    }
    Tablet (String Correo_electronico,String Nombre_del_Dispositivo,boolean Visible,boolean Encendido){
        super(Correo_electronico, Nombre_del_Dispositivo, Visible, Encendido);
        this.setTipo(2);
    }

    @Override
    public String Copiar_texto() {
        String textoCopiado="";
        if(this.estaEncendido()){
              Scanner scan= new Scanner(System.in);
               System.out.println("ingrese el texto a copiar");
               textoCopiado=scan.nextLine();
        }else{
            System.out.println("Dispositivo no puede realizar esta accion");
        }
        return textoCopiado;
    }

    @Override
    public void Pegar_texto(String texto_copiado) {
        if(this.estaEncendido()){
            if(this.esVisible()){
               System.out.println("texto copiado: " +texto_copiado);  
            }else{
               System.out.println("Dispositivo no puede mostrar esta accion");
            }  
        }else{
            System.out.println("Dispositivo no puede realizar esta accion");
        }
    }

    @Override
    public String Compartir_documentos() {
       String nombreDocumento="";
        if(this.estaEncendido()){
              Scanner scan= new Scanner(System.in);
               System.out.println("Ingrese el nombre del documento que desea compartir");
               nombreDocumento=scan.nextLine();
        }else{
            System.out.println("Dispositivo apagado");
        }
        return nombreDocumento;
    }
}
