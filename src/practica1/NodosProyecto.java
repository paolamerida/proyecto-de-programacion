
package practica1;

/**
 *
 * @author Pao
 */
public class NodosProyecto {
    private NodosProyecto Siguiente;
    private Dispositivo dispositivo;
    private NodosProyecto Anterior;

    public NodosProyecto(NodosProyecto Anterior,Dispositivo dispositivo, NodosProyecto Siguiente) {
        this.Anterior = Anterior;
        this.Siguiente = Siguiente;
        this.dispositivo = dispositivo;
        
    }

    public NodosProyecto() {
    }


    public NodosProyecto getAnterior() {
        return Anterior;
    }

    public void setAnterior(NodosProyecto Anterior) {
        this.Anterior = Anterior;
    }

    public NodosProyecto getSiguiente() {
        return Siguiente;
    }

    public void setSiguiente(NodosProyecto Siguiente) {
        this.Siguiente = Siguiente;
    }

    public Dispositivo getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(Dispositivo dispositivo) {
        this.dispositivo = dispositivo;
    }
  
    public boolean  tiene_siguiente(){
        return Siguiente!=null;
    }
    public boolean  tiene_Anterior(){
        return Anterior!=null;
    }

    
    
    
    
    
}
