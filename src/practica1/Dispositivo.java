
package practica1;

/**
 *
 * @author Pao
 */
public abstract class  Dispositivo {
    private String  Correo_electronico;
    private String  Nombre_del_Dispositivo;
    private boolean Visible;
    private boolean Encendido;
    private int Tipo;
    
    Dispositivo(){
        this.Correo_electronico="";
        this.Nombre_del_Dispositivo="";
        this.Visible=false;
        this.Encendido=false;
    }
    
    Dispositivo(String Correo_electronico,String Nombre_del_Dispositivo,boolean Visible,boolean Encendido){
        this.Correo_electronico=Correo_electronico;
        this.Nombre_del_Dispositivo=Nombre_del_Dispositivo;
        this.Visible=Visible;
        this.Encendido=Encendido;
    }
    public String getCorreo_electronico() {
        return Correo_electronico;
    }

    public void setCorreo_electronico(String Correo_electronico) {
        this.Correo_electronico = Correo_electronico;
    }

    public String getNombre_del_Dispositivo() {
        return Nombre_del_Dispositivo;
    }

    public void setNombre_del_Dispositivo(String Nombre_del_Dispositivo) {
        this.Nombre_del_Dispositivo = Nombre_del_Dispositivo;
    }

    public boolean esVisible() {
        return Visible;
    }

    public void setVisible(boolean Visible) {
        this.Visible = Visible;
    }

    public boolean estaEncendido() {
        return Encendido;
    }

    public void setEncendido(boolean Encendido) {
        this.Encendido = Encendido;
    }

    public int getTipo() {
        return Tipo;
    }

    public void setTipo(int Tipo) {
        this.Tipo = Tipo;
    }
    
    
   public  void solicitar_fotografia(String nombreFotografia){
        System.out.println("Dispositivo: "+ this.getNombre_del_Dispositivo()+ " recibiendo fotografia con nombre " + nombreFotografia);
   }
   
   public abstract String Copiar_texto();
   
   public abstract void Pegar_texto(String texto_copiado); 
   
   public void llamadas_telefonicas(){
   
   }
   public void Mensaje(String mensaje){
       System.out.println("dispositivo "+ this.getNombre_del_Dispositivo()+ " Muestra Mensaje " +mensaje);
   }
   public void Notificaciones(String notificacion){
        System.out.println("dispositivo "+ this.getNombre_del_Dispositivo()+ " Muestra Notificacion " +notificacion);
   }
   public abstract String Compartir_documentos();
   
   public void  Auriculares(){
   
   }
   public void Reproducir_musica(){
   
   }
    
}
