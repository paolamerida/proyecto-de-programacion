
package practica1;

/**
 *
 * @author Pao
 */
public class ListasDoblementeEnlazadasProyecto {
    
  private NodosProyecto Inicio;
  private NodosProyecto Fin;
  private int Tamanio = 0;
  
  public boolean CrearNodo(Dispositivo dispositivo){
    NodosProyecto nodonuevo= new NodosProyecto(null,dispositivo,null);
    if (Inicio==null ){
        Inicio=nodonuevo;
        Fin=nodonuevo;
        Tamanio++;
        return true;
    }
    else {
    NodosProyecto Temporal = Inicio;
    while(Temporal.tiene_siguiente()){
        Temporal=Temporal.getSiguiente();
    }
    
    Temporal.setSiguiente(nodonuevo);
    nodonuevo.setAnterior(Temporal);
    Fin=nodonuevo;
    Tamanio++;
    return true;
    }
  }
  
  public void RecorrerLista(){
      System.out.println("se recorrio lista de izquierda a derecha ");
      NodosProyecto Temporal = Inicio;
     while(Temporal!=null){
         System.out.println(Temporal.getDispositivo());
        Temporal=Temporal.getSiguiente();
    }

  
  }
  public void RecorrerListaInversa(){
      System.out.println("se recorrio lista de derecha a izquierda ");
      NodosProyecto Temporal = Fin;
     while(Temporal!=null){
         System.out.println(Temporal.getDispositivo());
        Temporal=Temporal.getAnterior();
    }
  
  }    
    
    
}
