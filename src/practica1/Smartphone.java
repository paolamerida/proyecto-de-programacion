
package practica1;

import java.util.Scanner;

/**
 *
 * @author Pao
 */
public class Smartphone extends Dispositivo{
    private int Numero_de_telefono;
    
    Smartphone(){
        super();
        this.setTipo(4);
    }
    Smartphone(String Correo_electronico,String Nombre_del_Dispositivo,boolean Visible,boolean Encendido,int Numero_de_telefono){
        super(Correo_electronico, Nombre_del_Dispositivo, Visible, Encendido);
        this.setNumero_de_telefono(Numero_de_telefono);
        this.setTipo(4);
    }

    public int getNumero_de_telefono() {
        return Numero_de_telefono;
    }

    public void setNumero_de_telefono(int Numero_de_telefono) {
        this.Numero_de_telefono = Numero_de_telefono;
    }

    @Override
    public String Copiar_texto() {
        String textoCopiado="";
        if(this.estaEncendido()){
              Scanner scan= new Scanner(System.in);
               System.out.println("ingrese el texto a copiar");
               textoCopiado=scan.nextLine();
        }else{
            System.out.println("Dispositivo no puede realizar esta accion");
        }
        return textoCopiado;
    }

    @Override
    public void Pegar_texto(String texto_copiado) {
        if(this.estaEncendido()){
            if(this.esVisible()){
               System.out.println("texto copiado: " +texto_copiado);  
            }else{
               System.out.println("Dispositivo no puede mostrar esta accion");
            }  
        }else{
            System.out.println("Dispositivo no puede realizar esta accion");
        }
    }

    @Override
    public String Compartir_documentos() {
       String nombreDocumento="";
        if(this.estaEncendido()){
              Scanner scan= new Scanner(System.in);
               System.out.println("Ingrese el nombre del documento que desea compartir");
               nombreDocumento=scan.nextLine();
        }else{
            System.out.println("Dispositivo apagado");
        }
        return nombreDocumento;
    }
    
    public String TomarFotografia(){
        String nombrFotografia="";
        Scanner scan= new Scanner(System.in);
        System.out.println("Ingrese el nombre de la fotografia");
        nombrFotografia=scan.nextLine();
        System.out.println("Smartphone "+ this.getNombre_del_Dispositivo()+ " Toma Fotografia");
        System.out.println( "Enviando fotografia....");
        return nombrFotografia;
    }
    
    
}