
package practica1;
import java.io.IOException;
import java.lang.IllegalStateException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner; 
import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
/**
 *
 * @author Pao
 */
public class Practica1 {
    static Dispositivo [] datos=new Dispositivo[100]; 
    static int contador=0;
    static String texto_copiado="";
    static objeto_log[] bitacora=new objeto_log[500];
    static int contador_log=0;
    /**
     * @param args the command line arguments
     */
    public static void guardarlog(objeto_log log){
        bitacora[contador_log]=log;
        contador_log++;
    }
    
    public static void main(String[] args) {
        
        Login login = new Login();
        login.setVisible(true);

        
    }
    
    public static void menuPrincipal(){
        Scanner scan= new Scanner(System.in);
        int opcion;
        boolean salida=false;
        while(!salida){
            System.out.println("----------- Ecosistema de Dispositivos -----------:");
            System.out.println("1. Crear dispositivo");
            System.out.println("2. Administrar dispositivos");
            System.out.println("3. Acciones con dispositivos");
            System.out.println("4. Acciones extras de dispositivos");
            System.out.println("5. Cargas masivas");
            System.out.println("6. Logs ");
            System.out.println("7 Salida"); 
            System.out.println(" ");
             System.out.println(" Seleccione la opcion a ejecutar: ");
            opcion=scan.nextInt();
            switch(opcion){
                case 1:{ 
                     menuCrearDispositivo();
                    break;
                }
                case 2:{
                    menuAdministrarDispositivo();
                    break;
                }
                case 3:{
                    menuAcciones_con_Dispositivos();
                    break;
                }
                case 4:{
                    menuAccionesExternas_de_Dispositivos();
                    break;
                }
                case 5:{
                    menuCargaMasiva();
                    break;
                }
                case 6:{
                    Generar_bitacora();
                    break;
                }
                case 7:{
                    salida=true;// salir
                    System.exit(1);
                    break;
                }
                default:{
                    
                    break;
                }
            } 
        } 
    }
  
    public static void menuCrearDispositivo(){
     Scanner scan= new Scanner(System.in);
        int opcion;
        boolean salida=false;
        while(!salida){
            System.out.println("----------- Crear Dispositivo -----------:");
            System.out.println("1. Computadora portatil");
            System.out.println("2. Tablet");
            System.out.println("3. Smartwatch");
            System.out.println("4. Smartphone");
            System.out.println("5. Auriculares inalambricos");
            System.out.println("6 Salida"); 
            System.out.println(" ");
             System.out.println(" Seleccione tipo de dispositivo a crear: ");
            opcion=scan.nextInt();
            switch(opcion){
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                {
                    CrearDispositivo(opcion);
                    break;
                }
                case 6:{
                    salida=true;// salir
                    break;
                }
                default:{
                    
                    break;
                }
            } 
        } 
        
    }
    
    public static void CrearDispositivo(int tipoDispositivo){
            Scanner teclado = new Scanner(System.in);
            
            switch(tipoDispositivo){
                case 1:{
                    Computadora compu= new Computadora();
                    System.out.println("----------- Creacion de Computadora-----------:");
                    System.out.println("1. Correo electronico:");
                    compu.setCorreo_electronico(teclado.nextLine());
                    System.out.println("2. Nombre del dispositivo");
                    compu.setNombre_del_Dispositivo(teclado.nextLine());
                    System.out.println("3. visible para conexion:");
                    String visible=teclado.nextLine();
                    if(visible.equalsIgnoreCase("Si")){
                        compu.setVisible(true);
                    }else{
                        compu.setVisible(false);
                    }
                    System.out.println("4. por default quedara Encendido");
                    compu.setEncendido(true);
                    datos[contador]=compu; 
                    contador++;    
                    Date fecha = new Date();
                    objeto_log log=new objeto_log("Creacion  de dispositivo", "Manual","se creo computadora con nombre "+compu.getNombre_del_Dispositivo(),fecha);
                    guardarlog(log);
                    break; 
                }
                case 2:{
                    Tablet tablet= new Tablet();
                    System.out.println("----------- Creacion de nueva tablet-----------:");
                    System.out.println("1. Correo electronico:");
                    tablet.setCorreo_electronico(teclado.nextLine());
                    System.out.println("2. Nombre del dispositivo");
                    tablet.setNombre_del_Dispositivo(teclado.nextLine());
                    System.out.println("3. visible para conexion:");
                    String visible=teclado.nextLine();
                    if(visible.equalsIgnoreCase("Si")){
                        tablet.setVisible(true);
                    }else{
                        tablet.setVisible(false);
                    }
                    System.out.println("4. por default quedara Encendido");
                    tablet.setEncendido(true);
                    datos[contador]=tablet;
                    contador++;
                    Date fecha = new Date();
                    objeto_log log=new objeto_log("Creacion  de dispositivo", "Manual","se creo Tablet con nombre "+tablet.getNombre_del_Dispositivo(),fecha);
                    guardarlog(log);
                    break;
                }
                case 3:{
                    Smartwatch smartwatch= new Smartwatch();
                    System.out.println("----------- Creacion de Smartwatch-----------:");
                    System.out.println("1. Correo electronico:");
                    smartwatch.setCorreo_electronico(teclado.nextLine());
                    System.out.println("2. Nombre del dispositivo");
                    smartwatch.setNombre_del_Dispositivo(teclado.nextLine());
                    System.out.println("3. visible para conexion:");
                    String visible=teclado.nextLine();
                    if(visible.equalsIgnoreCase("Si")){
                        smartwatch.setVisible(true);
                    }else{
                        smartwatch.setVisible(false);
                    }
                    System.out.println("4. por default quedara Encendido");
                    smartwatch.setEncendido(true);
                    datos[contador]= smartwatch;
                    contador++;
                    Date fecha = new Date();
                    objeto_log log=new objeto_log("Creacion  de dispositivo", "Manual","se creo smartwatch con nombre "+smartwatch.getNombre_del_Dispositivo(),fecha);
                    guardarlog(log);
                    break;
                }
                case 4:{
                    Smartphone smartphone= new Smartphone();
                    System.out.println("----------- Creacion de Smartphone-----------:");
                    System.out.println("1. Correo electronico:");
                    smartphone.setCorreo_electronico(teclado.nextLine());
                    System.out.println("2. Nombre del dispositivo");
                    smartphone.setNombre_del_Dispositivo(teclado.nextLine());
                    System.out.println("3. visible para conexion:");
                    String visible=teclado.nextLine();
                    if(visible.equalsIgnoreCase("Si")){
                        smartphone.setVisible(true);
                    }else{
                        smartphone.setVisible(false);
                    }
                    System.out.println("4. Numero de telefono");
                    smartphone.setNumero_de_telefono(teclado.nextInt()); 
                    System.out.println("5. por default quedara Encendido");
                    smartphone.setEncendido(true);
                    datos[contador]= smartphone;
                    contador++;
                    Date fecha = new Date();
                    objeto_log log=new objeto_log("Creacion  de dispositivo", "Manual","se creo smartphone con nombre "+smartphone.getNombre_del_Dispositivo(),fecha);
                    guardarlog(log);
                    break;
                }
                case 5:{
                    Auriculares auriculares= new Auriculares();
                    System.out.println("----------- Creacion de Auriculares inalambricos-----------:");
                    
                    break;
                }
            }
            
    }
 
    public static void menuAdministrarDispositivo(){
        Scanner scan= new Scanner(System.in);
        int opcion;
        boolean salida=false;
        while(!salida){
            System.out.println("----------- Administrar Dispositivos -----------:");
            System.out.println("1. Computadora portatil");
            System.out.println("2. Tablet");
            System.out.println("3. Smartwatch");
            System.out.println("4. Smartphone");
            System.out.println("5. Auriculares inalambricos");
            System.out.println("6 Salida"); 
            System.out.println(" ");
             System.out.println(" Seleccione tipo de dispositivo a administrar: ");
            opcion=scan.nextInt();
            switch(opcion){
                case 1:{
                    AdministrarDispositivo(opcion,"Computadora");
                    break;
                }
                case 2:{
                    AdministrarDispositivo(opcion,"Tablet");
                    break;
                }
                case 3:{
                    AdministrarDispositivo(opcion,"SmartWatch");
                    break;
                }
                case 4:{
                    AdministrarDispositivo(opcion,"SmartPhone");
                    break;
                }
                case 5:
                {
                    break;
                }
                case 6:{
                    salida=true;// salir
                    break;
                }
                default:{
                    
                    break;
                }
            } 
        } 
    }
    
    public static void AdministrarDispositivo(int tipoDispositivo, String titulo){
        
        System.out.println("----------- Administrar " + titulo + "-----------:");
        int [] listaPosiciones= new int[100];
        int contador= 0;
        for (int i = 0; i < 100; i++) {
           Dispositivo dispositivo= datos[i];
           if(dispositivo!=null){
                if(dispositivo.getTipo()==tipoDispositivo){
                    String estado="Apagado";
                    if(dispositivo.estaEncendido()){
                        estado="Encendido";
                    }
                    listaPosiciones[contador]=i;
                   contador++;
                   System.out.println((contador)+ "-" + dispositivo.getNombre_del_Dispositivo()+ "     " + estado ); 
                }
           }
        }
        System.out.println("Seleccione el Dispositivo a administrar: ");
        Scanner scan= new Scanner(System.in);
        int opcion=scan.nextInt();
        Dispositivo dispositivoModificar= datos[listaPosiciones[opcion-1]];
        if(dispositivoModificar!=null){
            System.out.println("----------- Administracion de " + titulo + " -----------:");
            System.out.println(" ");
            System.out.println(" Nombre del dispositivo seleccionado: " + dispositivoModificar.getNombre_del_Dispositivo());
            System.out.println(" ");
            System.out.println("1. Editar Correo Electronico");
            System.out.println("2. Editar Nombre del Dispositivo");
            System.out.println("3. Apagar/Encender visibilidad del dispositivo");
            System.out.println("4. Apagar/Encender dispositivo");
            System.out.println(" ");
            System.out.println(" Seleccione la propiedad a editar: ");
            opcion=scan.nextInt();
            switch(opcion){
                case 1:{
                    System.out.println("Escriba el nuevo Correo Electronico");
                    scan= new Scanner(System.in);
                    String nuevoCorreo=scan.nextLine();
                    dispositivoModificar.setCorreo_electronico(nuevoCorreo);
                    System.out.println("se realizo el cambio del correo electronico");
                    Date fecha = new Date();
                    objeto_log log=new objeto_log("Administracion de dispositivo", "Manual","se modifica correo del dispositivo: "+dispositivoModificar.getNombre_del_Dispositivo(),fecha);
                    guardarlog(log);
                break;
                }
                case 2:{
                    System.out.println("Escriba el nuevo Nombre del dispositivo");
                    scan= new Scanner(System.in);
                    String nuevoNombre=scan.nextLine();
                    dispositivoModificar.setNombre_del_Dispositivo(nuevoNombre);
                    System.out.println("se realizo el cambio de nombre de dispositivo");
                    Date fecha = new Date();
                    objeto_log log=new objeto_log("Administracion de dispositivo", "Manual","se modifica Nombre del dispositivo: "+dispositivoModificar.getNombre_del_Dispositivo(),fecha);
                    guardarlog(log);
                break;
                }
                case 3:{
                    if(dispositivoModificar.esVisible()){
                        dispositivoModificar.setVisible(false);
                        System.out.println("se apago la visibilidad del dispositivo");
                        Date fecha = new Date();
                        objeto_log log=new objeto_log("Administracion de dispositivo", "Manual","se apago visibilidad del dispositivo: "+dispositivoModificar.getNombre_del_Dispositivo(),fecha);
                        guardarlog(log);
                    }else{
                      dispositivoModificar.setVisible(true);
                      System.out.println("se encendio la visibilidad del dispositivo");
                      Date fecha = new Date();
                      objeto_log log=new objeto_log("Administracion de dispositivo", "Manual","se encendio la visibilidad del dispositivo: "+dispositivoModificar.getNombre_del_Dispositivo(),fecha);
                      guardarlog(log);
                    }
                break;
                }
                case 4:{
                    if(dispositivoModificar.estaEncendido()){
                        dispositivoModificar.setEncendido(false);
                        System.out.println("se apago el dispositivo");
                        Date fecha = new Date();
                       objeto_log log=new objeto_log("Administracion de dispositivo", "Manual","se apago el dispositivo: "+dispositivoModificar.getNombre_del_Dispositivo(),fecha);
                       guardarlog(log);
                    }else{
                      dispositivoModificar.setEncendido(true);
                      System.out.println("se encendio el dispositivo");
                      Date fecha = new Date();
                      objeto_log log=new objeto_log("Administracion de dispositivo", "Manual","se encendio el dispositivo: "+dispositivoModificar.getNombre_del_Dispositivo(),fecha);
                      guardarlog(log);
                    }
                break;
                }
            }

        }
            
    }
   
    public static void menuAcciones_con_Dispositivos(){
        Scanner scan= new Scanner(System.in);
        int opcion;
        boolean salida=false;
        while(!salida){
            System.out.println("----------- Seleccione el tipo de dispositivo: -----------:");
            System.out.println("1. Computadora portatil");
            System.out.println("2. Tablet");
            System.out.println("3. Smartwatch");
            System.out.println("4. Smartphone");
            System.out.println("5. Auriculares inalambricos");
            System.out.println("6 Salida"); 
            System.out.println(" ");
             System.out.println(" Seleccione un  tipo de dispositivo: ");
            opcion=scan.nextInt();
            switch(opcion){
                case 1:{
                    AccionesConDispositivos(opcion,"Computadora");
                    break;
                }
                case 2:{
                    AccionesConDispositivos(opcion,"Tablet");
                    break;
                }
                case 3:{
                    AccionesConDispositivos(opcion,"SmartWatch");
                    break;
                }
                case 4:{
                    AccionesConDispositivos(opcion,"SmartPhone");
                    break;
                }
                case 5:
                {
                    break;
                }
                case 6:{
                    salida=true;// salir
                    break;
                }
                default:{
                    
                    break;
                }
            } 
        } 
    }
    
    public static void AccionesConDispositivos(int tipoDispositivo,String titulo ){
        System.out.println("----------- Acciones " + titulo + "-----------:");
        int [] listaPosiciones= new int[100];
        int contador= 0;
        for (int i = 0; i < 100; i++) {
           Dispositivo dispositivo= datos[i];
           if(dispositivo!=null){
                if(dispositivo.getTipo()==tipoDispositivo){
                    String estado="Apagado";
                    if(dispositivo.estaEncendido()){
                        estado="Encendido";
                    }
                    listaPosiciones[contador]=i;
                   contador++;
                   System.out.println((contador)+ "-" + dispositivo.getNombre_del_Dispositivo()+ "     " + estado ); 
                }
           }
        }
        System.out.println("Seleccione el Dispositivo: ");
        Scanner scan= new Scanner(System.in);
        int opcion=scan.nextInt();
        Dispositivo accion_dispositivo= datos[listaPosiciones[opcion-1]];
        if(accion_dispositivo!=null){
            System.out.println("----------- Acciones de " + titulo + " -----------:");
            System.out.println(" ");
            System.out.println(" Tipo de dispositivo: " + titulo);
            System.out.println(" Nombre del dispositivo: " + accion_dispositivo.getNombre_del_Dispositivo());
            System.out.println(" ");
            System.out.println("1. Tomar fotografia");
            System.out.println("2. Copiar texto");
            System.out.println("3. Pegar texto");
            System.out.println("4. Compartir documento");
            System.out.println("5. Reproducir musica");
            System.out.println(" ");
            System.out.println(" Seleccione accion a Realizar: ");
            opcion=scan.nextInt();
            switch(opcion){
                case 1:{
                    Smartphone smarthpone = null;
                    for (int i = 0; i < 100; i++) {
                        Dispositivo dispotivo= datos[i];
                        if(dispotivo!=null){
                             if(dispotivo.getTipo()==4){
                                 if(dispotivo.getCorreo_electronico().equalsIgnoreCase(accion_dispositivo.getCorreo_electronico())){
                                     smarthpone = (Smartphone)dispotivo;
                                     break;
                                }
                            }
                        }
                    }
                    if(smarthpone!=null){
                        if(smarthpone.estaEncendido()){
                            if(smarthpone.esVisible()){
                                String nombrefoto = smarthpone.TomarFotografia();
                                Date fecha = new Date();
                                objeto_log log=new objeto_log("Accion de dispositivo", 
                                                               "Manual",
                                                               "Smarthpone "+smarthpone.getNombre_del_Dispositivo() +" toma y envia fotografia con nombre "+ nombrefoto,
                                                                fecha);
                                guardarlog(log);
                                accion_dispositivo.solicitar_fotografia(nombrefoto);
                                fecha = new Date();
                                 log=new objeto_log("Accion de dispositivo", 
                                                               "Manual",
                                                               "Dispositivo "+accion_dispositivo.getNombre_del_Dispositivo() +" solicita y recibe fotografia con nombre "+ nombrefoto,
                                                                fecha);
                                guardarlog(log);
                            }else{
                              System.out.println("Smartphone no visible");  
                            }
                        }else{
                            System.out.println("Smartphone apagado");
                        }
                        
                    }else{
                        System.out.println("No existe ningun smartphone asociado");
                    }
                    break;
                }
                case 2:{
                    texto_copiado=accion_dispositivo.Copiar_texto();
                    Date fecha = new Date();
                    objeto_log log=new objeto_log("Accion de dispositivo", "Manual","se copia texto del dispositivo: "+accion_dispositivo.getNombre_del_Dispositivo(),fecha);
                    guardarlog(log);
                    break;
                }
                case 3:{
                    accion_dispositivo.Pegar_texto(texto_copiado);
                    texto_copiado="";
                    Date fecha = new Date();
                    objeto_log log=new objeto_log("Accion de dispositivo", "Manual","se pega texto al dispositivo: "+accion_dispositivo.getNombre_del_Dispositivo(),fecha);
                    guardarlog(log);
                    break;
                }
                case 4:{
                    String documento = accion_dispositivo.Compartir_documentos();
                    if(!documento.isEmpty()){
                        Dispositivo dispositivoDestino = obtenerDispositivo();
                        if(dispositivoDestino!=null){
                            Date fecha = new Date();
                            objeto_log log=new objeto_log("Accion de dispositivo", 
                                                "Manual",
                                                "Dispositivo "+accion_dispositivo.getNombre_del_Dispositivo() +" comparte documento "+ documento,
                                                 fecha);
                            guardarlog(log);
                            String mensaje="El dispositivo "+accion_dispositivo.getNombre_del_Dispositivo()+" comparte documento "+ documento + " Aceptar ?" ;
                            dispositivoDestino.Mensaje(mensaje);
                            fecha = new Date();
                            log=new objeto_log("Accion de dispositivo", 
                                                "Manual",
                                                "Mensaje a dispositivo "+dispositivoDestino.getNombre_del_Dispositivo() +" Mensaje : "+ mensaje,
                                                 fecha);
                            guardarlog(log);
                        }
                        
                    }
                    break;
                }
            }

        }
    }
    
    public static Dispositivo obtenerDispositivo(){
        Dispositivo accion_dispositivo = null;
        int [] listaPosiciones= new int[100];
        int contador= 0;
        System.out.println("----DISPOSITVOS VISIBLES----");
        for (int i = 0; i < 100; i++) {
           Dispositivo dispositivo= datos[i];
           if(dispositivo!=null){
                if(dispositivo.esVisible()){
                    String estado="Visible";
                    listaPosiciones[contador]=i;
                   contador++;
                   System.out.println((contador)+ "-" + dispositivo.getNombre_del_Dispositivo()+ "     " + estado ); 
                }
           }
        }
        System.out.println("Seleccione el Dispositivo: ");
        Scanner scan= new Scanner(System.in);
        int opcion=scan.nextInt();
        accion_dispositivo = datos[listaPosiciones[opcion-1]];
        return accion_dispositivo;
    }
    
    public static Dispositivo obtenerDispositivoxNombre(String nombre){
        Dispositivo dispositivo = null;
        for (int i = 0; i < 100; i++) {
           Dispositivo Dispositivotmp= datos[i];
           if(Dispositivotmp!=null){
                if(Dispositivotmp.getNombre_del_Dispositivo().equals(nombre)){
                    dispositivo = Dispositivotmp;
                }
           }
        }
        return dispositivo;
    }
    
    public static void replicarMensajes(String mensaje, String correo,int Tipo){
        for (int i = 0; i < 100; i++) {
           Dispositivo Dispositivotmp= datos[i];
           if(Dispositivotmp!=null){
                if(Dispositivotmp.getCorreo_electronico().equals(correo)){
                    if(Tipo==1){
                        Dispositivotmp.Mensaje(mensaje);
                        Date fecha = new Date();
                        objeto_log log=new objeto_log("Accion de dispositivo", 
                                            "Manual",
                                            "Mensaje a dispositivo "+Dispositivotmp.getNombre_del_Dispositivo() +" Mensaje : "+ mensaje,
                                             fecha);
                        guardarlog(log);
                    }else{
                        Dispositivotmp.Notificaciones(mensaje);
                        Date fecha = new Date();
                        objeto_log log=new objeto_log("Accion de dispositivo", 
                                            "Manual",
                                            "Notificacion a dispositivo "+Dispositivotmp.getNombre_del_Dispositivo() +" Notificacion : "+ mensaje,
                                             fecha);
                         guardarlog(log);
                    }
                    
                }
           }
        }
    }
    
    public static void menuAccionesExternas_de_Dispositivos(){
        Scanner scan= new Scanner(System.in);
        int opcion;
        boolean salida=false;
        while(!salida){
            System.out.println("----------- Acciones Externas De Dispositivos : -----------:");
            System.out.println(" ");
            System.out.println("Seleccione el tipo de Accion a realizar :");
            System.out.println("1. llamada telefonica");
            System.out.println("2. Mensaje");
            System.out.println("3. Notificacion");
            System.out.println("4 Salida"); 
            System.out.println(" ");
             System.out.println(" Seleccione accion a realizar: ");
            opcion=scan.nextInt();
            switch(opcion){
                case 1:{
                    
                    break;
                }
                case 2:{
                    System.out.println("Escriba el  Nombre del dispositivo");
                    scan= new Scanner(System.in);
                    String nombre=scan.nextLine();
                    Dispositivo dispositivoAccion = obtenerDispositivoxNombre(nombre);
                    System.out.println(" ");
                    System.out.println(" ------Tipo de Mensaje -----");
                    System.out.println("1. Mensaje de Texto");
                    System.out.println("2. Mensaje de red Social");
                    System.out.println("3. Mensajeria Instantanea");
                    System.out.println("4  Mensaje de videoconferencia"); 
                    System.out.println(" ");
                    System.out.println(" Seleccione el tipo de Mensjae: ");
                    scan= new Scanner(System.in);
                    opcion=scan.nextInt();
                    switch(opcion){
                        case 1:{ 
                            System.out.println("Escriba el texto del mensaje");
                            scan= new Scanner(System.in);
                            String texto=scan.nextLine();
                            dispositivoAccion.Mensaje(texto);
                            Date fecha = new Date();
                            objeto_log log=new objeto_log("Accion de dispositivo", 
                                                "Manual",
                                                "Mensaje a dispositivo "+dispositivoAccion.getNombre_del_Dispositivo() +" Mensaje : "+ texto,
                                                 fecha);
                            guardarlog(log);
                            replicarMensajes(texto,dispositivoAccion.getCorreo_electronico(),1);
                            break;
                        }
                        case 2:{ 
                            System.out.println("Escriba el texto del mensaje");
                            scan= new Scanner(System.in);
                            String texto=scan.nextLine();
                            System.out.println("Escriba la red social");
                            scan= new Scanner(System.in);
                            String redSocial=scan.nextLine();
                            String Mensaje = "Red Social : " + redSocial  + " Mensaje: " +texto;
                            dispositivoAccion.Mensaje(Mensaje);
                            Date fecha = new Date();
                            objeto_log log=new objeto_log("Accion de dispositivo", 
                                                "Manual",
                                                "Mensaje a dispositivo "+dispositivoAccion.getNombre_del_Dispositivo() +" Mensaje : "+ Mensaje,
                                                 fecha);
                            guardarlog(log);
                            replicarMensajes(Mensaje,dispositivoAccion.getCorreo_electronico(),1);
                            break;
                        }
                        case 3:{ 
                            System.out.println("Escriba el texto del mensaje");
                            scan= new Scanner(System.in);
                            String texto=scan.nextLine();
                            System.out.println("Escriba la aplicacion de mensajeria instantanea");
                            scan= new Scanner(System.in);
                            String aplicacion=scan.nextLine();
                            System.out.println("Escriba el nombre de quien envia mensaje");
                            scan= new Scanner(System.in);
                            String nombrePersona=scan.nextLine();
                            String Mensaje = "Aplicacion de mensajeria instantanea : " + aplicacion + " Nombre de la persona que envia mensaje : " + nombrePersona + " Mensaje: " +texto;
                            dispositivoAccion.Mensaje(Mensaje);
                            Date fecha = new Date();
                            objeto_log log=new objeto_log("Accion de dispositivo", 
                                                "Manual",
                                                "Mensaje a dispositivo "+dispositivoAccion.getNombre_del_Dispositivo() +" Mensaje : "+ Mensaje,
                                                 fecha);
                            guardarlog(log);
                            replicarMensajes(Mensaje,dispositivoAccion.getCorreo_electronico(),1);
                            break;
                        }
                        case 4:{
                            System.out.println("Escriba el texto del mensaje");
                            scan= new Scanner(System.in);
                            String texto=scan.nextLine();
                            System.out.println("Escriba la aplicacion de videoconferencia");
                            scan= new Scanner(System.in);
                            String aplicacion=scan.nextLine();
                            System.out.println("Escriba el nombre de quien envia mensaje");
                            scan= new Scanner(System.in);
                            String nombrePersona=scan.nextLine();
                            String Mensaje = "Aplicacion de videoconferencia: " + aplicacion + " Nombre de la persona que envia mensaje : " + nombrePersona + " Mensaje: " +texto;
                            dispositivoAccion.Mensaje(Mensaje);
                            Date fecha = new Date();
                            objeto_log log=new objeto_log("Accion de dispositivo", 
                                                "Manual",
                                                "Mensaje a dispositivo "+dispositivoAccion.getNombre_del_Dispositivo() +" Mensaje : "+ Mensaje,
                                                 fecha);
                            guardarlog(log);
                            replicarMensajes(Mensaje,dispositivoAccion.getCorreo_electronico(),1);
                            break;
                        }
                    }
                    break;
                }
                case 3:
                {
                    System.out.println("Escriba el  Nombre del dispositivo");
                    scan= new Scanner(System.in);
                    String nombre=scan.nextLine();
                    Dispositivo dispositivoAccion = obtenerDispositivoxNombre(nombre);
                    System.out.println(" ");
                    System.out.println(" ------Tipo de Notificacion -----");
                    System.out.println("1. Correo electronico");
                    System.out.println("2. Evento de calendario");
                    System.out.println(" ");
                    System.out.println(" Seleccione el tipo de Notificacion: ");
                    scan= new Scanner(System.in);
                    opcion=scan.nextInt();
                     switch(opcion){
                        case 1:{  
                            System.out.println("Escriba el asunto del correo");
                            scan= new Scanner(System.in);
                            String asunto=scan.nextLine();
                            dispositivoAccion.Notificaciones(asunto);
                            Date fecha = new Date();
                            objeto_log log=new objeto_log("Accion de dispositivo", 
                                                "Manual",
                                                "Notificacion a dispositivo "+dispositivoAccion.getNombre_del_Dispositivo() +" Notificacion : "+ asunto,
                                                 fecha);
                            guardarlog(log);
                            replicarMensajes(asunto,dispositivoAccion.getCorreo_electronico(),2);
                            break;
                        }
                        case 2:{
                            System.out.println("Escriba la fecha del evento");
                            scan= new Scanner(System.in);
                            String fechaEvento=scan.nextLine();
                            System.out.println("Escriba la hora del evento");
                            scan= new Scanner(System.in);
                            String hora=scan.nextLine();
                            System.out.println("Escriba el nombre del Evento");
                            scan= new Scanner(System.in);
                            String nombre_evento=scan.nextLine();
                            
                            String Notificacion = "Nombre del Evento "+ nombre_evento + " fecha " + fechaEvento  + " Hora" + hora;
                            dispositivoAccion.Notificaciones(Notificacion);
                            Date fecha = new Date();
                            objeto_log log=new objeto_log("Accion de dispositivo", 
                                                "Manual",
                                                "Notificacion a dispositivo "+dispositivoAccion.getNombre_del_Dispositivo() +" Notificacion : "+ Notificacion,
                                                 fecha);
                            guardarlog(log);
                            replicarMensajes(Notificacion,dispositivoAccion.getCorreo_electronico(),2);
                            break;
                        }
                     }
                    break;
                }
                case 4:{
                    salida=true;// salir
                    break;
                }
                default:{
                    break;
                }
            } 
        } 
    
    }
    
    public static void AccionesExternas_de_Dispositivos(int tipoDispositivo){
    switch(tipoDispositivo){
            case 1:{
                System.out.println("----------- LLamada telefonica-----------:");
                break;
            }
            case 2:{
               System.out.println("----------- Mensaje-----------:");
                break;
            }
            case 3:{
                System.out.println("---------- Notificacion-----------:");
                break;
            }
        }
    }
    
    public static void menuCargaMasiva(){
      Scanner scan= new Scanner(System.in);
        int opcion;
        boolean salida=false;
        while(!salida){
            System.out.println("----------- Carga Masiva : -----------:");
            System.out.println("1. Cargar dispositivos");
            System.out.println("2. Administrar dispositivos");
            System.out.println("3. cargar acciones con dispositivos");
            System.out.println("4. cargar acciones externas de  dispositivos");
            System.out.println("5. Salida"); 
            System.out.println(" ");
             System.out.println(" Seleccione accion a realizar: ");
            opcion=scan.nextInt();
            switch(opcion){
                case 1:{
                    System.out.println("----------- carga Dispositivos -----------:");
                    CargarDispositivos();
                    break;
                }
                case 2:{
                    System.out.println("-----------  Administrar dispositivos -----------:");
                    CargaAdministrar_dispositivo();
                    break;
                }
                case 3:{
                    System.out.println("---------- Cargar acciones con dispositivos -----------:");
                    break;
                }
                case 4:{
                    System.out.println("---------- Cargar acciones externas de dispositivos-----------:");
                    break;
                }
                case 5:{
                    salida=true;// salir
                    break;
                }
                default:{
                    
                    break;
                }
            } 
        } 
    }
    
    public static void CargarDispositivos(){
    
        try
        {
            // abre el archivo
            Scanner entrada;
            Scanner scan= new Scanner(System.in);
            
            System.out.println("ingrese la ruta del archivo");
            String ruta= scan.nextLine();
            entrada = new Scanner(Paths.get(ruta));// abre el archivo
            // lee el archivo
            while (entrada.hasNextLine()) // mientras haya otra linea más qué leer
            {
           
               String[] partes_de_la_linea =entrada.nextLine().split(","); // lee linea por linea, separa la linea por comas
               String tipo_dispositivo = partes_de_la_linea[0]; // coloca la primera palabra de la linea en la posicion 0 del arreglo
               switch(tipo_dispositivo){ // valida que tipo de dispositivo es y toma el nombre del dispositivo
                   
                   case "computadora portatil":{
                       Computadora nueva_computadora= new Computadora();
                       nueva_computadora.setCorreo_electronico(partes_de_la_linea[3]);
                       nueva_computadora.setNombre_del_Dispositivo(partes_de_la_linea[4]);
                       String visible=partes_de_la_linea[5];
                        if(visible.equalsIgnoreCase("Si")){
                            nueva_computadora.setVisible(true);
                        }else{
                            nueva_computadora.setVisible(false);
                        }
                        nueva_computadora.setEncendido(true);
                        datos[contador]=nueva_computadora; 
                        contador++;
                        Date fecha = new Date();
                        objeto_log log=new objeto_log("Creacion  de dispositivo", "Archivo","se creo computadora portatil con nombre "+nueva_computadora.getNombre_del_Dispositivo(),fecha);
                        guardarlog(log);
                       
                   break;
                   }
                   case "tablet":{
                       Tablet nueva_tablet= new Tablet();
                       nueva_tablet.setCorreo_electronico(partes_de_la_linea[3]);
                       nueva_tablet.setNombre_del_Dispositivo(partes_de_la_linea[4]);
                       String visible=partes_de_la_linea[5];
                        if(visible.equalsIgnoreCase("Si")){
                            nueva_tablet.setVisible(true);
                        }else{
                            nueva_tablet.setVisible(false);
                        }
                        nueva_tablet.setEncendido(true);
                        datos[contador]=nueva_tablet; 
                        contador++;
                        Date fecha = new Date();
                        objeto_log log=new objeto_log("Creacion  de dispositivo", "Archivo","se creo tablet con nombre "+nueva_tablet.getNombre_del_Dispositivo(),fecha);
                        guardarlog(log);
                   break;
                   }
                   case "smartwatch":{
                       Smartwatch nuevo_Smartwatch= new Smartwatch();
                       nuevo_Smartwatch.setCorreo_electronico(partes_de_la_linea[3]);
                       nuevo_Smartwatch.setNombre_del_Dispositivo(partes_de_la_linea[4]);
                       String visible=partes_de_la_linea[5];
                        if(visible.equalsIgnoreCase("Si")){
                            nuevo_Smartwatch.setVisible(true);
                        }else{
                            nuevo_Smartwatch.setVisible(false);
                        }
                        nuevo_Smartwatch.setEncendido(true);
                        datos[contador]=nuevo_Smartwatch; 
                        contador++;
                        Date fecha = new Date();
                        objeto_log log=new objeto_log("Creacion  de dispositivo", "Archivo","se creo smartwatch con nombre "+nuevo_Smartwatch.getNombre_del_Dispositivo(),fecha);
                        guardarlog(log);
                    break;
                    }
                    case "smartphone":{
                       Smartphone nuevo_Smarphone= new Smartphone();
                       nuevo_Smarphone.setNumero_de_telefono(Integer.parseInt(partes_de_la_linea[1]));
                       nuevo_Smarphone.setCorreo_electronico(partes_de_la_linea[3]);
                       nuevo_Smarphone.setNombre_del_Dispositivo(partes_de_la_linea[4]);
                       String visible=partes_de_la_linea[5];
                        if(visible.equalsIgnoreCase("Si")){
                            nuevo_Smarphone.setVisible(true);
                        }else{
                            nuevo_Smarphone.setVisible(false);
                        }
                        nuevo_Smarphone.setEncendido(true);
                        datos[contador]=nuevo_Smarphone; 
                        contador++;
                        Date fecha = new Date();
                        objeto_log log=new objeto_log("Creacion  de dispositivo", "Archivo","se creo smartphone con nombre "+nuevo_Smarphone.getNombre_del_Dispositivo(),fecha);
                        guardarlog(log);
                        break;
                    } 
                    case"auriculares":{
                    
                        
                        break;
                    }
               }
            }
            System.out.println("Carga de dispositivos finalizada");
             entrada.close();// cierra el archivo
              
        } 
        catch(IOException iOException)
        {
            System.err.println("Error al abrir el archivo");
        }
        
    }
    
    public static void CargaAdministrar_dispositivo(){
         try
        {
            // abre el archivo
            Scanner entrada;
            Scanner scan= new Scanner(System.in);
            
            System.out.println("ingrese la ruta del archivo");
            String ruta= scan.nextLine();
            entrada = new Scanner(Paths.get(ruta));// abre el archivo
            // lee el archivo
            while (entrada.hasNextLine()) // mientras haya otra linea más qué leer
            {
                String[] partes_de_la_linea =entrada.nextLine().split(","); // lee linea por linea, separa la linea por comas
               String tipo_dispositivo = partes_de_la_linea[0]; // coloca la primera palabra de la linea en la posicion 0 del arreglo
               int tipoDispositivo=0;
               switch(tipo_dispositivo){ // valida que tipo de dispositivo es y toma el nombre del dispositivo
                   case "computadora portatil":{
                       tipoDispositivo=1;
                   break;
                   }
                   case "tablet":{
                       tipoDispositivo=2;
                   break;
                   }case "smartwatch":{
                       tipoDispositivo=3;
                   break;
                   }case "smartphone":{
                       tipoDispositivo=4;
                   break;
                   }
                   case "auriculares":{
                       tipoDispositivo=5;
                   break;
                   }
               }
               // busca dispositivo por nombre y edita los campos a modificar
                for (int i = 0; i < 100; i++) {
                    Dispositivo dispositivo= datos[i];
                    if(dispositivo!=null){
                         if(dispositivo.getTipo()==tipoDispositivo && dispositivo.getNombre_del_Dispositivo().equals(partes_de_la_linea[1])){
                             String campo_modificar=partes_de_la_linea[2];
                             switch(campo_modificar){
                                 case "nombre":{
                                     dispositivo.setNombre_del_Dispositivo(partes_de_la_linea[3]);
                                     Date fecha = new Date();
                                     objeto_log log=new objeto_log("Administracion de dispositivo", "Archivo","se modifica nombre del dispositivo: "+dispositivo.getNombre_del_Dispositivo(),fecha);
                                     guardarlog(log);
                                     return;
                                 }
                                 case"correo electronico":{
                                     dispositivo.setCorreo_electronico(partes_de_la_linea[3]);
                                     Date fecha = new Date();
                                     objeto_log log=new objeto_log("Administracion de dispositivo", "Archivo","se modifica correo electronico del dispositivo: "+dispositivo.getNombre_del_Dispositivo(),fecha);
                                     guardarlog(log);
                                     return;
                                 }
                                 case"visible":{
                                     if(partes_de_la_linea[3].equalsIgnoreCase("verdadero")){
                                         dispositivo.setVisible(true);
                                         Date fecha = new Date();
                                         objeto_log log=new objeto_log("Administracion de dispositivo", "Archivo","se enciende visibilidad del dispositivo: "+dispositivo.getNombre_del_Dispositivo(),fecha);
                                         guardarlog(log);
                                     }else{
                                         dispositivo.setVisible(false);
                                         Date fecha = new Date();
                                         objeto_log log=new objeto_log("Administracion de dispositivo", "Archivo","se apaga visibilidad del dispositivo: "+dispositivo.getNombre_del_Dispositivo(),fecha);
                                         guardarlog(log);
                                     }
                                     return;
                                 }
                                 case"encender":{
                                     if(partes_de_la_linea[3].equalsIgnoreCase("verdadero")){
                                         dispositivo.setEncendido(true);
                                         dispositivo.setVisible(true);
                                         Date fecha = new Date();
                                         objeto_log log=new objeto_log("Administracion de dispositivo", "Archivo","se Enciende el dispositivo: "+dispositivo.getNombre_del_Dispositivo(),fecha);
                                         guardarlog(log);
                                     }else{
                                         dispositivo.setEncendido(false);
                                         dispositivo.setVisible(true);
                                         Date fecha = new Date();
                                         objeto_log log=new objeto_log("Administracion de dispositivo", "Archivo","se Apaga el dispositivo: "+dispositivo.getNombre_del_Dispositivo(),fecha);
                                         guardarlog(log);
                                     }
                                     return;
                                 }
                             }
                              
                         }
                    }
                }
               
            }
            System.out.println("Carga de administracion de dispositivos");
             entrada.close();// cierra el archivo
              
        } 
        catch(IOException iOException)
        {
            System.err.println("Error al abrir el archivo");
        }
    
    }
    
    public static void Generar_bitacora(){
        int tamaño=bitacora.length;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");  
        SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");  
      
        String texthtml = "";
        texthtml += "<html> <head>"
                + "<style>\n" +
                    "#customers {\n" +
                    "  font-family: Arial, Helvetica, sans-serif;\n" +
                    "  border-collapse: collapse;\n" +
                    "  width: 100%;\n" +
                    "}\n" +
                    "\n" +
                    "#customers td, #customers th {\n" +
                    "  border: 1px solid #ddd;\n" +
                    "  padding: 8px;\n" +
                    "}\n" +
                    "\n" +
                    "#customers tr:nth-child(even){background-color: #f2f2f2;}\n" +
                    "\n" +
                    "#customers tr:hover {background-color: #ddd;}\n" +
                    "\n" +
                    "#customers th {\n" +
                    "  padding-top: 12px;\n" +
                    "  padding-bottom: 12px;\n" +
                    "  text-align: left;\n" +
                    "  background-color: #4CAF50;\n" +
                    "  color: white;\n" +
                    "}\n" +
                    "</style>"
                + " <h1> LOGS </h1> </head>";
        texthtml += "<body> <table id=\"customers\">";
        texthtml += "<tr>";
        texthtml += "<th> Numero </th>";
        texthtml += "<th> Tipo de accion </th>";
        texthtml += "<th> Origen de la Accion </th>";
        texthtml += "<th> Descripcion </th>";
        texthtml += "<th> Fecha </th>";
        texthtml += "<th> Hora </th>";
        texthtml += "</tr>";
        for (int i = 0; i < tamaño; i++) {
            objeto_log log = bitacora[i];
            if (log!=null){
                texthtml += "<tr>";
                texthtml += "<td>"+ (i+1) +"</td>";
                texthtml += "<td>"+log.getTipo_accion()+"</td>";
                texthtml += "<td>"+log.getOrigen_accion()+"</td>";
                texthtml += "<td> "+log.getDescripcion()+" </td>";
                texthtml += "<td> "+ formatter.format(log.getFecha())+" </td>";
                texthtml += "<td> "+ formatter2.format(log.getFecha())+" </td>";
                texthtml += "</tr>";

            }
        }
        texthtml += "</table></body>";
        texthtml += "</html>";
        
        try {
            String ruta = "bitacora.html";
            File file = new File(ruta);
            // Si el archivo no existe es creado
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(texthtml);
            bw.close();
            Desktop.getDesktop().open(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }                         

}
