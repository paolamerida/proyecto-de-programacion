
package practica1;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
/**
 *
 * @author Pao
 */
public class SQLclass {
    public SQLclass(){
    }
    public Statement conn(){
    try {
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        System.out.println("Connecting to the database...");
        Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","APP_DISPOSITIVOS","mipc1234");
        Statement statement=connection.createStatement();
        return statement;
        } catch(Exception e){
            System.out.println("the exception raised is :" + e);
            return null;
        }
    }
    public LinkedList<String> query(String from){
        try{
            Statement st=conn();
            ResultSet resultSet= st.executeQuery(from);
            
            LinkedList<String>result = new LinkedList();
            while(resultSet.next()){
                for (int i=1; i <= resultSet.getMetaData().getColumnCount(); i++){
                result.add(resultSet.getString(i));
                }
            }
            return result;
        } catch (Exception e){
            return null;
        }
    }
    
    public int insertActualizarData(String consulta){
        try{
            Statement st=conn();
            int i = st.executeUpdate(consulta);
            return i;
        } catch (Exception e){
            return -1;
        }
    }
    public  ResultSet getTabla(String from){
        try{
            Statement st=conn();
            ResultSet resultSet= st.executeQuery(from);
            return resultSet;
        } catch (Exception e){
            return null;
        }
    }
}
