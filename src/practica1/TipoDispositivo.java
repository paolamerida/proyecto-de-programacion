/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

/**
 *
 * @author Pao
 */
public class TipoDispositivo {
    private int idTipoDispostivo;
    private String tipoDispositivo;

    public TipoDispositivo(int idTipoDispostivo, String tipoDispositivo) {
        this.idTipoDispostivo = idTipoDispostivo;
        this.tipoDispositivo = tipoDispositivo;
    }

    public int getIdTipoDispostivo() {
        return idTipoDispostivo;
    }

    public void setIdTipoDispostivo(int idTipoDispostivo) {
        this.idTipoDispostivo = idTipoDispostivo;
    }

    public String getTipoDispositivo() {
        return tipoDispositivo;
    }

    public void setTipoDispositivo(String tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
    }
   
    @Override
     public String toString()
    {
     return tipoDispositivo;
    }
    
    
}
